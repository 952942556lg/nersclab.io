# Contribution guide

This guide outlines how to contribute to this project and standards
which should be observed when adding to this repository.

## About

This repository contains NERSC technical documentation written in
Markdown which is converted to html/css/js with the
[mkdocs](http://www.mkdocs.org) static site generator. The theme
is based on [mkdocs-material](https://github.com/squidfunk/mkdocs-material)
with NERSC customizations such as the colors.

## Rules

1. Do not commit large files (e.g. very high-res images, binary data, executables).
1. Follow the existing style of Markdown.
1. All changes are to be submitted as Merge Requests.

## Prerequisites

* Anaconda or Python virtual env
* git
* gitlab.com account

## Web GUI

Gitlab.com provides on online Markdown editor. For some *minor*
changes the full site generator is not required: e.g fixing a
typo. Even minor changes must be submitted via Merge Request.

When browsing docs.nersc.gov there is a pencil icon next to the header
of each page. This pencil is a link to edit the current page. When
using this feature make sure to update the commit message (required)
and new branch name (suggested).

## git branches and forks

For more substantial changes it is recommended to develop changes in
either a fork or branch. Creating new branches is restricted to NERSC
staff only.

### obtain a copy of the repository

If NERSC staff and working with a branch

```
git clone git@gitlab.com:NERSC/nersc.gitlab.io.git
```

or

```
git clone https://gitlab.com/NERSC/nersc.gitlab.io.git
```

Otherwise, fork the repository and clone the fork.

### Install dependencies

You will need to install some dependencies in order to build
documentation locally. This step is optional but highly
encouraged. This example uses a conda environment, but you are free to
choose any python3 environment.
 
```
cd nersc.gitlab.io
conda create -n docs pip
conda activate docs
pip install -r requirements.txt
```

### Edit with live preview

Open a terminal session with the appropriate conda environment
activated, navigate to the root directory of the repository (where
`mkdocs.yml` is located) and run the command `mkdocs serve`. This will
start a live-reloading local web server. You can then open
[http://127.0.0.1:8000](http://127.0.0.1:8000) in a web browser to
see your local copy of the site.

In another terminal (or with a GUI editor) edit existing files, add
new pages to `mkdocs.yml`, etc. As you save your changes the local
web serve will automatically re-render and reload the site.

### Output a static site

To build a self-contained directory containing the full html/css/js
of the site:

```
mkdocs build
```

### Typical workflow

Working with a branch of the main repo.

1.  Make a new branch and call it something descriptive.

    ```shell
    git checkout -b username/what_you_are_doing
    ```

2.  Create/edit your content
3.  Commit your changes

    ```
	git add <files I changed>
    git commit -m 'describe what I did'
    ```

4.  Push your changes to gitlab

    ```
    git push
    ```

    Or if the branch doesn't exist on the gitlab repository yet

    ```
    git push --set-upstream origin username/my-new-feature
    ```

5.  Check if the continuous integration of your changes was successful
    
    * It is possible the GitLab shared runners will fail for an opaque 
      reason (e.g. issues with the cloud provider where they are hosted).
      Hitting the "Retry" button for specific stages in the
      pipeline in the GitLab.com GUI may resolve this in some cases.

6.  Submit a merge request to the master branch with your changes

## Guidelines

### Adding new pages

For a newly added page to appear in the navigation edit the top-level
`mkdocs.yml` file. If you are unsure about the best location for a new
page please open an issue or (if staff) start a discussion in #docs on
nersc.slack.com. Note that duplication of information in discouraged,
so enhancing an existing page may be preferable to creating a new
page.

### Command prompts

#### with output

When showing a command and sample result, include a prompt and use
the `console` annotation for the code block.

```console
$ sqs
JOBID   ST  USER   NAME         NODES REQUESTED USED  SUBMIT               PARTITION SCHEDULED_START      REASON
864933  PD  elvis  first-job.*  2     10:00     0:00  2018-01-06T14:14:23  regular   avail_in_~48.0_days  None
```

#### without output

Commands shown without output can omit the prompt to facilitate
copy/paste.

```
sqs
```

#### placeholders

* `<must supply a value>`
  
  Example: In `ssh_job <jobid>` `<jobid>` is required.
  
* `[optional value]`
  
  Example 1: `iris [-h]` The `-h` is optional.
  
  Example 2: `iris user [username]` The `[username]` is optional.
	
* `{choice1|choice2|choice3}` choose one from set of items
  
  Example 1: `iris {user|qos|project}` Can only choose one of the
  valid sub-commands.
  
  Example 2: `sbatch --qos={debug|regular|premium}`. Can only choose
  one QOS.

* `<value>...` Ellipsis for items that can be repeated.
  
  Example: `ls [file]...` It is allowed to add any number of "file"
  arguments ie `ls afile anotherfile another`. This is optional since
  `ls` works without any parameters so `[]` is used.


### Identifiable information 

Where possible, replace the username, project name, etc with clearly
arbitrary fake data. For usernames `elvis` is a popular choice:

```console
$ ls -l
total 23
drwxrwx--- 2 elvis elvis  512 Jan  5 13:56 accounts
drwxrwx--- 3 elvis elvis  512 Jan  5 13:56 data
drwxrwx--- 3 elvis elvis  512 Jan  9 15:35 demo
drwxrwx--- 2 elvis elvis  512 Jan  5 13:56 img
-rw-rw---- 1 elvis elvis  407 Jan  9 15:35 index.md
[snip]
```
	
### Writing Style

When adding a page think about your audience.

* Are they new or advanced expert users?
* What is the goal of this content?

* [Grammatical Person](https://en.wikiversity.org/wiki/Technical_writing_style#Grammatical_person)
* [Active Voice](https://en.wikiversity.org/wiki/Technical_writing_style#Use_active_voice)

### `bash` not `csh`

All examples should be given in `bash` when possible.

### Terms

* I/O not IO
* NERSC Project not NERSC repo.

### ambiguous terms

In some cases a term may be used differently in related contexts. Be
sure to clarify when there could be doubt.

#### Examples

* Slurm allocation vs NERSC allocation
* Slurm account vs NERSC username

### Slurm options

* Show both long and short option when introducing an option in text
* Use the long version (where possible) in scripts
* Special highlighting of Slurm scripts is available with "slurm" 
  annotation of code blocks.

## How to

### Review a Merge Request from a private fork

1.  Modify `.git/config` so merge requests are visible

    ```text
    ...
    [remote "origin"]
	        url = git@gitlab.com:NERSC/documentation.git
	        fetch = +refs/heads/*:refs/remotes/origin/*
	        fetch = +refs/merge-requests/*/head:refs/remotes/origin/pr/*
	...
	```

2.  Check for any Merge Requests

    ```shell
    git fetch
    ```

3.  Checkout a specific Merge Request for review (merge request `N`
	in this example)

    ```shell
    git checkout origin/pr/N
    ```

### Install and run markdown lint

In the base directory of the repository run `npm install
markdownlint-cli`. (You may need to
install [nodejs](https://nodejs.org/en/)).

```
cd nersc.gitlab.io
npm install markdownlint-cli
```

The linter is now installed locally and can be run

```
$ ./node_modules/markdownlint-cli/markdownlint.js docs/index.md
docs/index.md: 8: MD009/no-trailing-spaces Trailing spaces [Expected: 0 or 2; Actual: 1]
docs/index.md: 9: MD009/no-trailing-spaces Trailing spaces [Expected: 0 or 2; Actual: 1]
```

!!! note
	It is important to run the linter from the base directory so
	that the correct configuration file (`.markdownlint.json`) is
	used.
