# Current known issues

1. **`$CSCRATCH` interactive responsiveness**: Sometimes `$CSCRATCH`
   appears to hang for a while on commands like `ls`. This is mostly due
   to I/O load on the file system. Using `/bin/ls` instead of `ls` can help
   if your `ls` is aliased to something that requires every files to be
   inspected, such as `ls --color`. Limiting the number of files in a
   single directory to hundreds, rather than thousands, can also help.

