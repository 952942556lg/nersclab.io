# Cori Large Memory Nodes

Cori has a set of 20 nodes, each with 2 TB of memory and a 3.0 GHz
AMD EPYC 7302 (Rome) processor. These nodes are being installed and
configured in August 2020, and are expected to be available to users
in September. The nodes are available to high-priority scientific
or technical campaigns that have a special need for this hardware.
The initial focus is on supporting COVID-19 related research and
preparing for the Perlmutter system (which will have a similar AMD
processor).

## Node Features

Each node contains

-  One 16-core AMD EPYC 7302 (Rome) processor running at 3.0 GHz
-  2 TB of RAM
-  3.2 TB of NVMe SSD local scratch disk, mounted as `/tmp`

## File Systems

The usual Cori file systems will be available, including

-  Home Directories
-  Community File System (CFS)
-  Scratch (`$CSCRATCH`)

Each node also has

-  A 3.2 TB local `/tmp` SSD partition

## Network

The large memory nodes are not connected to Cori’s Aries high speed
network. Multi-node applications can use Open MPI to communicate
over an InfiniBand network.

## Programming Environment

The user environment is similar to a Cori login node. However, the
large memory nodes have AMD processors, unlike Cori, which has Intel
processors. You will need to (re)compile your codes to run on the
AMD hardware.
