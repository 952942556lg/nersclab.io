# Programming Environment Change on Cori in July 2018

## Background

Following the scheduled maintenances on Cori (July 11), the machines will return to service with a new default programmming environment. 

During the maintenance, we will change the default Cray Developer Toolkit (CDT) version from 17.09 to 18.03. CDT 17.09 will remain available, but the version that was default last year, 17.06, will be removed. In addition, we will install a future-looking version, 18.06.

As a consequence of this change, the default versions of many packages will change. The default version of the Intel compiler, however, will remain the same.

Below is the detailed list of changes after the scheduled maintenance on Cori.

## Software default version changes

* cce/8.6.5 (from 8.6.2)
* cray-fftw/3.3.6.3 (from 3.3.6.2)	
* cray-libsci/18.03.1 (from 17.09.1)
* cray-mpich, cray-mpich-abi, cray-shmem/7.7.0 (from 7.6.2)
* cray-netcdf, cray-netcdf-hdf5parallel/4.4.1.1.6 (from 4.4.1.1.3)
* cray-petsc, cray-petsc-64, cray-petsc-complex, cray-petsc-complex-64/3.7.6.2 (from 3.7.6.0)
* cray-tpsl, cray-tpsl-64/17.11.1 (from 17.06.1)
* cray-trilinos/12.10.1.2 (from 12.10.1.1)
* craype/2.5.14 (from 2.5.12)
* craype-ml-plugin-py2, craype-ml-plugin-py3/1.0.1 (from N/A)
* craypkg-gen/1.3.6 (from 1.3.5)
* gcc/7.3.0 (from 7.1.0)
* papi/5.5.1.4 (from 5.5.1.3)
* perftools, perftools-base, perftools-lite/7.0.0 (from	6.5.2)
* pmi, pmi-lib/5.0.13 (from 5.0.12)


## Software versions to be removed

* cce/8.6.0
* cray-lgdb/3.0.6
* cray-libsci/17.06.1
* cray-mpich, cray-mpich-abi, cray-shmem/7.6.0
* cray-python/17.06.1
* craype/2.5.11
* gcc/6.3.0
* papi/5.5.1.2
* perftools, perftools-base, perftools-lite/6.5.0
* stat/3.0.1.0


## New software versions available

* atp/2.1.2
* cce/8.7.1
* cray-ccdb/3.0.4
* cray-cti/1.0.7
* cray-fftw/3.3.6.5
* cray-ga/5.3.0.8
* cray-hdf5, cray-hdf5-parallel/1.10.2.0
* cray-lgdb/3.0.9
* cray-libsci/18.04.1
* cray-mpich, cray-mpich-abi, cray-shmem/7.7.1
* cray-netcdf, cray-netcdf-hdf5parallel/4.6.1.0
* cray-petsc, cray-petsc-64, cray-petsc-complex, cray-petsc-complex-64/3.8.4.0
* cray-python/17.09.1
* cray-tpsl, cray-tpsl-64/18.06.1
* cray-trilinos/12.12.1.0
* craype/2.5.15
* craypkg-gen/1.3.7
* gdb4hpc/3.0.9
* papi/5.6.0.2
* perftools, perftools-base, perftools-lite/7.0.2
* pmi, pmi-lib/5.0.14
* stat/3.0.1.2
 